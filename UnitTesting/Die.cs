﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTesting
{
    public class Die
    {
        private static readonly Random rndm = new Random();

        public int Roll()
        {
            return rndm.Next(5) + 1;
        }
    }
}
